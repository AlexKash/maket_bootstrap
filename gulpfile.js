const gulp = require('gulp');
const sass = require('gulp-sass');
const image = require('gulp-image');
const concat = require('gulp-concat');
const pug = require('gulp-pug');
const browserSync = require('browser-sync');

gulp.task('image', function () {
    gulp.src('./src/assets/img/*')
        .pipe(image())
        .pipe(gulp.dest('./build/assets/img'));
});

gulp.task('vendors', function () {
    gulp.src('./vendors/**/*')
        .pipe(gulp.dest('./build/vendors'));
});

gulp.task('html', function () {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./build'));
});

gulp.task('sass', function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('./build/styles'));
});

gulp.task('browserSync', function () {
    browserSync({
        server: {
            baseDir: './build/'
        }
    })
});

gulp.task('watch', ['sass', 'html', 'vendors', 'image', 'browserSync'], function () {
    gulp.watch('./src/styles**/*.scss', ['sass']);
    gulp.watch('./src/assets/img/*', ['image']);
    gulp.watch('./src/**/*.html', ['html']);
    gulp.watch('build/*.html', browserSync.reload);
    gulp.watch("./build/**/*.css").on("change", browserSync.reload);
    gulp.watch('./build/**/*.js').on("change", browserSync.reload);
});

gulp.task('default', ['watch', 'image']);